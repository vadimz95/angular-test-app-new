export const API = {
  PATH_NAME: 'https://jsonplaceholder.typicode.com',
  ALL_POSTS: 'posts',
  ALL_POSTS_COMMENTS: 'posts/1/comments',
  COMMENTS_BY_POST_ID: 'comments',
  POSTS_BY_USER_ID: 'posts?userId=',
};
