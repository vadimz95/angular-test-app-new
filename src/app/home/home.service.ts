import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {API} from '../config/api.config';
import {HttpClient} from '@angular/common/http';

@Injectable()

export class HomeService {
  constructor(private http: HttpClient) {
  }

  public getAllPosts(): Observable<any> {
    return this.http.get(API.PATH_NAME + '/' + API.ALL_POSTS, {});
  }

  public getAllPostsComments(): Observable<any> {
    return this.http.get(API.PATH_NAME + '/' + API.ALL_POSTS_COMMENTS, {});
  }

  public getPostById(id: any): Observable<any> {
    return this.http.get(API.PATH_NAME + '/' + API.ALL_POSTS + '/' +  id);
  }

  public getCommentsByPostId(id: any): Observable<any> {
    return this.http.get(API.PATH_NAME + '/' + API.COMMENTS_BY_POST_ID + '?postId=' +  id);
  }
  public getPostsByUserId(id: any): Observable<any> {
    console.log('id', id);
    return this.http.get(API.PATH_NAME + '/' + API.POSTS_BY_USER_ID +  id);
  }
}
