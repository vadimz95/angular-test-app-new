import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.modules';
import {PostListComponent} from './post-list/post-list.component';
import {PostDetailsComponent} from './post-details/post-details.component';
import {MatCardModule, MatListModule} from '@angular/material';
import {HomeService} from './home.service';
import {HttpClientModule} from '@angular/common/http';
import {AuthorPostComponent} from './author-posts/author-post.component';

@NgModule({
  declarations: [
    HomeComponent,
    PostListComponent,
    PostDetailsComponent,
    AuthorPostComponent
  ],
  imports: [
    BrowserModule,
    HomeRoutingModule,
    MatCardModule,
    MatListModule,
    HttpClientModule,
  ],
  exports: [MatCardModule],
  providers: [HomeService],
})
export class HomeModule {
}
