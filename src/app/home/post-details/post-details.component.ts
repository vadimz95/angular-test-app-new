import {Component, OnInit} from '@angular/core';
import {HomeService} from '../home.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit {
  public post: any;
  public postComments: any;
  private id = this.route.snapshot.paramMap.get('id');

  constructor(private homeService: HomeService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.getPostById();
    this.getCommentsByPostId();
  }

  public getPostById(): any {
    this.homeService.getPostById(this.id)
      .subscribe(res => {
        this.post = res;
        console.log(this.post);
      });
  }

  public getCommentsByPostId(): any {
    this.homeService.getCommentsByPostId(this.id)
      .subscribe(res => {
        this.postComments = res;
        this.postComments.commentsLength = this.postComments.length;
        console.log(this.postComments);
      });
  }
}
