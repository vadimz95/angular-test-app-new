import {Component, OnInit} from '@angular/core';
import {HomeService} from '../home.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-author-page',
  templateUrl: './author-post.component.html',
  styleUrls: ['./author-post.component.scss']
})
export class AuthorPostComponent implements OnInit {
  private id = this.route.snapshot.params.userId;
  public allAuthorPosts;

  constructor(private homeService: HomeService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getPostsByUserId();
  }

  getPostsByUserId() {
    console.log(this.route.snapshot.params);
    this.homeService.getPostsByUserId(this.id)
      .subscribe(res => {
        this.allAuthorPosts = res;
        console.log(res);
      });
  }
}
