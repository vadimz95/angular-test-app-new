import {Component, OnInit} from '@angular/core';
import {HomeService} from '../home.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-user-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  public allPost: any;
  public allPostsComments: any;

  // public fullPostsList;

  constructor(private homeService: HomeService,
              private router: Router) {
  }

  ngOnInit() {
    this.getAllPosts();
    this.getAllPostsComments();
  }

  public getAllPosts(): any {
    this.homeService.getAllPosts()
      .subscribe(res => {
        this.allPost = res;
      });
  }

  public getAllPostsComments(): any {
    this.homeService.getAllPostsComments()
      .subscribe(res => {
        this.allPostsComments = res;
        this.postFormatter();
      });
  }

  public postFormatter(): any {
    this.allPostsComments.map(item => {
      this.allPost.reduce((iv, post) => {
        if (item.postId === post.id) {
        }
      });
    });
  }

  public goToDetailPost(path: number): void {
    this.router.navigate([`post/${path}`]);
  }

  public goToAuthorPost(path: number): void {
    this.router.navigate([`${path}`]);
  }
}
